﻿using System;  
using System.Collections.Generic;  
using System.Net;  
using System.Net.Sockets;  
using System.IO;  
using System.Text;  
using System.Threading;  
 
namespace Client  
{  
    class Program  
    {  
        static void Main(string[] args)  
        {  
            try  
            {  
                TcpClient tcpClient = new TcpClient("127.0.0.1", 8080);  
                Console.WriteLine("Connected to server.");  
                Console.WriteLine("");  
                Console.WriteLine("============PETUNJUK PERMAINAN===========");
                Console.WriteLine("> /START THE GAME! untuk memulai permainan");
                Console.WriteLine("> /MORE CLUE! untuk meminta tambahan petunjuk");
                Console.WriteLine("============SELAMAT BERMAIN===========");
 
                Thread thread = new Thread(Read);  
                thread.Start(tcpClient);  
 
                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());  
 
                while (true)  
                {  
                    if (tcpClient.Connected)  
                    {  
                        string input = Console.ReadLine();  
                        sWriter.WriteLine("client2 : " +input);  
                        sWriter.Flush();  
                    }  
                }  
 
            }  
            catch (Exception e)  
            {  
                Console.Write(e.Message);  
            }  
 
            Console.ReadKey();  
        }  
 
        static void Read(object obj)  
        {  
            TcpClient tcpClient = (TcpClient)obj;  
            StreamReader sReader = new StreamReader(tcpClient.GetStream());  
 
            while (true)  
            {  
                try  
                {  
                    string message = sReader.ReadLine();  
                    Console.WriteLine(message);  
                }  
                catch (Exception e)  
                {  
                    Console.WriteLine(e.Message);  
                    break;  
                }  
            }  
        }  
    }  
} 