﻿using System;  
using System.Collections.Generic;  
using System.Net;  
using System.Net.Sockets;  
using System.IO;  
using System.Text;  
using System.Threading;  
 
namespace Server  
{  
    class Program  
    {  
    	private static string word = string.Empty;
    	private static string clue = string.Empty;
    	private static string message = string.Empty;
        private static TcpListener tcpListener;  
        private static List<TcpClient> tcpClientsList = new List<TcpClient>();      
 
        static void Main(string[] args)  
        {  
        	tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
            tcpListener.Start();  
 
            Console.WriteLine("Server started");  
 
            while (true)  
            {  
                TcpClient tcpClient = tcpListener.AcceptTcpClient();  
                tcpClientsList.Add(tcpClient);  
 
                Thread thread = new Thread(ClientListener);  
                thread.Start(tcpClient);  
                  
            }  
        }  
 
        public static void ClientListener(object obj)  
        {  
            TcpClient tcpClient = (TcpClient)obj;  
            StreamReader reader = new StreamReader(tcpClient.GetStream());  
 
            Console.WriteLine("Client connected");  
            
 
            while (true)  
            {  
                message = reader.ReadLine(); 
                BroadCast(message, tcpClient);  
                Console.WriteLine(message);
       //         try
       //         {
                	if (message == "client1 : /START THE GAME!" || message == "client2 : /START THE GAME!" || message == "client3 : /START THE GAME!")
               		{	
                		gamePlay();
            		}  
                
                
            	else if (message == "client1 : /MORE CLUE!" || message == "client2 : /MORE CLUE!" || message == "client3 : /MORE CLUE!")
                	{
            			moreclue();
                	}
                
                
                else if (message == "client1 : " + word || message == "client2 : " + word || message == "client3 : " + word)
                	{
                		gameover();
                	}
      //          }
      //          catch (Exception e)  
      //      	{  
       //         Console.Write(e.Message);  
     //      		 }  
 
      //      Console.ReadKey();
              
        	}  
        }
        
            
        public static void BroadCast(string msg, TcpClient excludeClient)  
        {  
            foreach (TcpClient client in tcpClientsList)  
            {  
                if (client != excludeClient)  
                {  
                    StreamWriter sWriter = new StreamWriter(client.GetStream());  
                    sWriter.WriteLine(msg); 
                    sWriter.Flush();  

                }   
               
            }  
        }

        public static void gamePlay()
        {
        	foreach (TcpClient client in tcpClientsList)
            { 
        				StreamWriter sWriter = new StreamWriter(client.GetStream()); 
   						Console.WriteLine("enter the word");
           				word = Console.ReadLine();
           				int kata = word.Length;
            			sWriter.WriteLine("jumlah huruf : " + kata); 
            		
            			Console.WriteLine("enter clue");
            			clue = Console.ReadLine();
            			sWriter.WriteLine("petunjuk : " + clue);
            			sWriter.Flush();
        	}
        }
        
        public static void moreclue()
        {
        	foreach (TcpClient client in tcpClientsList)
            {
				     	StreamWriter sWriter = new StreamWriter(client.GetStream());
            			Console.WriteLine("enter clue");
            			clue = Console.ReadLine();
            			sWriter.WriteLine("petunjuk : " + clue);
            			sWriter.Flush();   		
        	}
        	
        }
        
        public static void gameover()
        {
        	foreach (TcpClient client in tcpClientsList)
            {
        				StreamWriter sWriter = new StreamWriter(client.GetStream());
            			sWriter.WriteLine("=======SELAMAT TEBAKAN ANDA BENAR!=======");
            			sWriter.WriteLine("> katanya adalah : " + word);
            			sWriter.WriteLine("=========================================");
            			sWriter.Flush(); 
        	}
        }
    }  
} 